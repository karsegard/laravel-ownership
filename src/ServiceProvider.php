<?php

namespace VendorName\Skeleton;

use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
/*HasFacade
use VendorName\Skeleton\Facades\FacadeName as Facade;
use VendorName\Skeleton\LibraryName as Library;
HasFacade*/
/*HasAssets    
    use KDA\Laravel\Traits\HasAssets;
HasAssets*/
/*HasComponents    
    use KDA\Laravel\Traits\HasComponents;
HasComponents*/
/*HasComponentNamespaces    
    use KDA\Laravel\Traits\HasComponentNamespaces;
HasComponentNamespaces*/
/*HasConfig    
    use KDA\Laravel\Traits\HasConfig;
HasConfig*/
/*HasDumps
    use KDA\Laravel\Traits\HasDumps;
HasDumps*/
/*HasDynamicSidebar
    use KDA\Laravel\Traits\HasDynamicSidebar;
HasDynamicSidebar*/
/*HasHelper     
    use KDA\Laravel\Traits\HasHelper;
HasHelper*/
/*HasLoadableMigration      
    use KDA\Laravel\Traits\HasLoadableMigration;
HasLoadableMigration*/
/*HasMigration      
    use KDA\Laravel\Traits\HasMigration;
HasMigration*/
/*HasObservers      
    use KDA\Laravel\Traits\HasObservers;
    HasObservers*/
/*HasProviders      
    use KDA\Laravel\Traits\HasProviders;
    HasProviders*/
/*HasFilamentProvider     
    use KDA\Laravel\Traits\HasProviders;
    HasFilamentProvider*/
/*HasPublicAssets       
    use KDA\Laravel\Traits\HasPublicAssets;
    HasPublicAssets*/
/*HasRoutes     
    use KDA\Laravel\Traits\HasRoutes;
    HasRoutes*/
/*HasTranslations       
    use KDA\Laravel\Traits\HasTranslations;
    HasTranslations*/
/*HasVendorViews        
    use KDA\Laravel\Traits\HasVendorViews;
    HasVendorViews*/
/*HasViews      
    use KDA\Laravel\Traits\HasViews;
    HasViews*/
/*HasYamlConfig     
    use KDA\Laravel\Traits\HasYamlConfig;
    HasYamlConfig*/
 /*ProvidesBlueprint     
    use KDA\Laravel\Traits\ProvidesBlueprint;
    ProvidesBlueprint*/
class ServiceProvider extends PackageServiceProvider
{
    /*HasAssets    
    use HasAssets;
HasAssets*/
    use HasCommands;
    /*HasComponents    
    use HasComponents;
HasComponents*/
    /*HasComponentNamespaces    
    use HasComponentNamespaces;
HasComponentNamespaces*/
    /*HasConfig    
    use HasConfig;
HasConfig*/
    /*HasDumps
    use HasDumps;
HasDumps*/
    /*HasDynamicSidebar
    use HasDynamicSidebar;
HasDynamicSidebar*/
    /*HasHelper     
    use HasHelper;
HasHelper*/
    /*HasLoadableMigration      
    use HasLoadableMigration;
HasLoadableMigration*/
    /*HasMigration      
    use HasMigration;
HasMigration*/
    /*HasObservers      
    use HasObservers;
    HasObservers*/
    /*HasProviders      
    use HasProviders;
    HasProviders*/
    /*HasFilamentProvider     
    use HasProviders;
    HasFilamentProvider*/
    /*HasPublicAssets       
    use HasPublicAssets;
    HasPublicAssets*/
    /*HasRoutes     
    use HasRoutes;
    HasRoutes*/
    /*HasTranslations       
    use HasTranslations;
    HasTranslations*/
    /*HasVendorViews        
    use HasVendorViews;
    HasVendorViews*/
    /*HasViews      
    use HasViews;
    HasViews*/
    /*HasYamlConfig     
    use HasYamlConfig;
    HasYamlConfig*/
    /*ProvidesBlueprint     
    use ProvidesBlueprint;
    ProvidesBlueprint*/

    protected $packageName =':package_name';

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    /*HasAssets
    // trait \KDA\Laravel\Traits\HasAssets; 
    // registers assets to be publishables
    // protected $assetsDir = 'assets';
HasAssets*/

    /*HasCommands
    // use \KDA\Laravel\Traits\HasCommands
    // registers package commands
    protected $_commands = [
        Commands\InstallCommand::class
    ];
HasCommands*/


    /*HasComponents  
    // trait \KDA\Laravel\Traits\HasComponents;
    protected $components = [
        'sc-render' => View\Components\Render::class,
    ];
HasComponents*/
    /*HasComponentNamespaces
    // trait \KDA\Laravel\Traits\HasComponentNamespaces;
    protected $componentNamespaces = [
        'View\\Components' => 'namespace'
    ];
HasComponentNamespaces*/

    /*HasComponentNamespaces    
    protected $anonymousComponentNamespaces = [
        'hello.world.components' => 'namespace'
    ];
HasComponentNamespaces*/

    /*HasConfig
    
     // trait \KDA\Laravel\Traits\HasConfig; 
     //    registers config file as 
     //      [file_relative_to_config_dir => namespace]
     
    protected $configDir='config';
    protected $configs = [
         ':vendor_slug/:package_name.php'  => ':vendor_slug.:package_name'
    ];
HasConfig*/

    /*HasDumps
     // trait:  \KDA\Laravel\Traits\HasDumps
     //  put the tables that should be included in a dump
     //  requires: fdt2k/laravel-database-dump        
     
    protected $dumps = [];
HasDumps*/

/*HasDynamicSidebar
    // trait \KDA\Laravel\Traits\HasDynamicSidebar
    // requires: fdt2k/backpack-dynamic-sidebar
    // registers dynamic sidebars as 
    //  [
    //      route_name => Translation Label
    //  ]
    protected $sidebars = [
        [
            'label' => 'Navigation',
            'route' => 'scmnavigation',
            'behavior' => 'hide',
            'icon' => 'la-route'
        ]
    ];
HasDynamicSidebar*/

    /*HasHelper
    // trait \KDA\Laravel\Traits\HasHelper
    // registers helpers files
    
    // protected $helperDir = 'helpers';
HasHelper*/

/*HasLoadableMigration
    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations

    // protected $migrationDir = 'database/migrations';
HasLoadableMigration*/

    /*HasMigration
    // trait use \KDA\Laravel\Traits\HasMigration
    //  registers publishable migrations
    // protected $migrationDir = 'database/migrations';
HasMigration*/

    /*HasObservers
     // trait  \KDA\Laravel\Traits\HasObservers
     // register model observers
     // [MODEL_CLASS=> OBSERVER_CLASS ]
    protected $observers = [
      \KDA\Example\Models\Example::class=> \KDA\Example\Observers\Example::class  
    ];
HasObservers*/
    /*HasProviders
     // trait  \KDA\Laravel\Traits\HasProviders
     // registers additionnal providers
    // [PROVIDER_CLASS]
     
    protected $additionnalProviders=[
        \KDA\Example\Providers\SomeProvider::class
    ];
HasProviders*/

    /*HasFilamentProvider
    //register filament provider
    protected $additionnalProviders=[
        \VendorName\Skeleton\FilamentServiceProvider::class
    ];
HasFilamentProvider*/

    /*HasPublicAssets
     // trait \KDA\Laravel\Traits\HasPublicAssets
     protected $publicDir = 'public';
HasPublicAssets*/

    /*HasRoutes
    // trait  \KDA\Laravel\Traits\HasRoutes
    // register routes, 
    protected $routesDir = 'routes';
    protected $routes = [
        'backpack/structured_editor.php'
    ];
HasRoutes*/

    /*HasTranslations
    
    //  trait  \KDA\Laravel\Traits\HasTranslations
    // register translations from directory (default: resources/lang)
      
     
    protected $translationsDir = '';
    protected $publishTranslationsTo= '';
HasTranslations*/

 /*HasVendorViews   
    //  trait \KDA\Laravel\Traits\HasVendorViews
    //  register another set of views, used if we supercharge another package views so we don't mix up with
    // potential published view 
     
    protected $vendorViewsNamespace = 'kda-sidebar';
    protected $publishVendorViewsTo = 'vendor/kda/backpack/sidebar';
HasVendorViews*/

/*HasViews
    //trait \KDA\Laravel\Traits\HasViews
     
    protected $viewNamespace = 'kda-sidebar';
    protected $publishViewsTo = 'vendor/backpack/base';
HasViews*/

/*HasYamlConfig
     // trait \KDA\Laravel\Traits\HasYamlConfig
     // allow use of config as yaml file
     // use : pragmarx/yaml
     // same variables than HasConfig
HasYamlConfig*/



    public function register()
    {
        parent::register();
/*HasFacade    
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
HasFacade*/
    }



    /**
     * called after the trait were registered
     */

    
    public function postRegister(){

/*ProvidesBlueprint
     // trait \KDA\Laravel\Traits\ProvidesBlueprint
     // Blueprint can be defined here
        $this->blueprints =[
            'nestable'=> function($parent_id_name='parent_id',$constraint=NULL,$onDelete='restrict'){
                $table = $constraint ?? $this->table;
                $this->foreignId($parent_id_name)->nullable()->constrained($table)->onDelete($onDelete);
                $this->unsignedInteger('lft')->nullable()->default(0);
                $this->unsignedInteger('rgt')->nullable()->default(0);
                $this->unsignedInteger('depth')->nullable()->default(0);
            }
        ];
ProvidesBlueprint*/
    }




    //called after the trait were booted
    protected function bootSelf(){
   /*HasBladeDirective
        Blade::directive('bind_values', function ($content) {
            return "<?php 
                foreach (\$content  as \$field => \$value) {
                    \$\$field = \$value;
                }
    
            ?>";
        });
HasBladeDirective*/
    }
}
